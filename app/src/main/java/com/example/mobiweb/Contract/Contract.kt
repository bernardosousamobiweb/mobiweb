package com.example.mobiweb.Contract

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobiweb.Account.Account_Details
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.R
import kotlinx.android.synthetic.main.contractual_info.*
import java.io.Serializable


class Contract : BaseActivity() , Serializable {

    private var contractDetails: Account_Details = Account_Details()

    companion object {
        const val CONTRACT_JSON = "contract"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contractual_info)

        val intent = intent
        intent.getSerializableExtra(CONTRACT_JSON).let {
            contractDetails = it as Account_Details
        }

        btnCopyStartDate.setOnClickListener{copyText(txtStartDate.text as String) }
        btnCopyEndDate.setOnClickListener{copyText(txtEndDate.text as String) }
        btnCopyRenovationDate.setOnClickListener{ copyText(txtRenovationDate.text as String) }

        val radius = this.resources.getDimensionPixelSize(R.dimen.corner_radius)
        contractDetails.avatarPath.let { value ->
            Glide.with(this)
                .load(value)
                .transform(RoundedCorners(radius))
                .error(R.drawable.dashaccount2)
                .into(avatarCon)
        }

        contractDetails.role?.let { value ->
            txtRoleCon.text = value
            layoutRoleCon.isVisible = value.isNotEmpty()
        }

        contractDetails.gender?.let { value ->
            txtGenderCon.text = value
            layoutGenderCon.isVisible = value.isNotEmpty()
        }

        contractDetails.country?.let { value ->
            txtCountryCon.text = value
            layoutCountryCon.isVisible = value.isNotEmpty()
        }

        contractDetails.contract?.let { value ->
            txtContract.text = value
            layoutContract.isVisible = value.isNotEmpty()
        }

        contractDetails.salary?.let { value ->
            txtSalary.text = value
            layoutSalary.isVisible = value.isNotEmpty()
        }

        contractDetails.startDate?.let { value ->
            txtStartDate.text = value
            layoutStartDate.isVisible = value.isNotEmpty()
        }

        contractDetails.endDate?.let { value ->
            txtEndDate.text = value
            layoutEndDate.isVisible = value.isNotEmpty()
        }

        contractDetails.renovationDate?.let { value ->
            txtRenovationDate.text = value
            layoutRenovationDate.isVisible = value.isNotEmpty()
        }

                    txtProfileCon.text = contractDetails.fullName
                    txtProfilePositionCon.text = contractDetails.category
                    progressBar4.visibility = View.GONE

                }
}