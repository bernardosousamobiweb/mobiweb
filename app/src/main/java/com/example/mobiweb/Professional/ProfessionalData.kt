package com.example.mobiweb.Professional

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.mobiweb.Account.Account_Details
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.R
import kotlinx.android.synthetic.main.professional_data.*
import java.io.Serializable

class ProfessionalData : BaseActivity(), Serializable {
    private var professionalDetails: Account_Details = Account_Details()

    companion object {
        const val PROFESSIONAL_JSON = "professional"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.professional_data)

        //region CLIPBOARD
        btnCopyInsuranceName.setOnClickListener{copyText(txtInsuranceCompany.text as String) }
        btnCopyInsurancePolicy.setOnClickListener{copyText(txtInsurancePolicy.text as String) }
        btnCopyInsuranceExpirationDate.setOnClickListener{ copyText(txtInsuranceExpirationDate.text as String) }
        btnCopyEmployeeCodePro.setOnClickListener{copyText(txtEmployeeCodePro.text as String) }
        //endregion CLIPBOARD

        val intent = intent
        intent.getSerializableExtra(PROFESSIONAL_JSON).let {
            professionalDetails = it as Account_Details
        }




        val radius = this.resources.getDimensionPixelSize(R.dimen.corner_radius)

                    //region GET_INFORMATION
                    professionalDetails.avatarPath.let { value ->
                        Glide.with(this)
                            .load(value)
                            .transform(RoundedCorners(radius))
                            .error(R.drawable.dashaccount2)
                            .into(avatarPro)
                    }

                    professionalDetails.role.let { value ->
                        txtRolePro.text = value
                        layoutRolePro.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.gender.let { value ->
                        txtGenderPro.text = value
                        layoutGenderPro.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.country.let { value ->
                        txtCountryPro.text = value
                        layoutCountryPro.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.category.let { value ->
                        txtCategory.text = value
                        layoutCategory.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.department.let { value ->
                        txtDepartment.text = value
                        layoutDepartment.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.ensuranceCompany.let { value ->
                        txtInsuranceCompany.text = value
                        layoutInsuranceCompany.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.insurancePolicy.let { value ->
                        txtInsurancePolicy.text = value
                        layoutInsurancePolicy.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.ensuranceExpirationDate.let { value ->
                        txtInsuranceExpirationDate.text = value
                        layoutInsuranceExpirationDate.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.workOffice.let { value ->
                        txtWorkOffice.text = value
                        layoutWorkOffice.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.schoolHabilitations.let { value ->
                        txtSchoolHabilitations.text = value
                        layoutSchoolHabilitations.isVisible = !value.isNullOrEmpty()
                    }

                    professionalDetails.employeeCode.let { value ->
                        txtEmployeeCodePro.text = value
                        layoutEmployeeCodePro.isVisible = !value.isNullOrEmpty()
                    }

                    txtProfilePro.text = professionalDetails.fullName
                    txtProfilePositionPro.text = professionalDetails.category
                    progressBar3.visibility = View.GONE
                    //endregion

                }
}