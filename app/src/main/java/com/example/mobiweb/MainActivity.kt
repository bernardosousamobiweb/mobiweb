package com.example.mobiweb

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.*
import com.example.mobiweb.Menu.Menu
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.login.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var handler: Handler
    private lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        val attrib = window.attributes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }

        //region ANIMATIONS
        val loginLabel = AnimationUtils.loadAnimation(this, R.anim.login_anim)
        val editTexts = AnimationUtils.loadAnimation(this, R.anim.et_login)
        val btLogin = AnimationUtils.loadAnimation(this, R.anim.btn_login)
        val ic_mw = findViewById<ImageView>(R.id.imgView_logo)
        val ed_login = findViewById<EditText>(R.id.etUsername)
        val ed_password = findViewById<EditText>(R.id.etPassword)
        val lb_login = findViewById<TextView>(R.id.tv_login)
        val lb_username = findViewById<TextView>(R.id.lblUsername)
        val lb_password = findViewById<TextView>(R.id.lblPassword)
        val bt_login = findViewById<Button>(R.id.loginButton)
        //endregion ANIMATIONS

        //region HANDLER
        handler = Handler()
        runnable = Runnable {
            rellay1.visibility = View.VISIBLE
            ic_mw.startAnimation(loginLabel)
            lb_login.startAnimation(loginLabel)
            lb_username.startAnimation(loginLabel)
            lb_password.startAnimation(loginLabel)
            ed_login.startAnimation(editTexts)
            ed_password.startAnimation(editTexts)
            bt_login.startAnimation(btLogin)
        }

        handler.postDelayed(runnable, 1200)
        //endregion HANDLER

        auth = FirebaseAuth.getInstance()

        //region LOGINBUTTON
        loginButton.setOnClickListener {
            signIn(etUsername.text.toString(), etPassword.text.toString())
        }
        //endregion

        //region EXIT_APPLICATION
        if (intent.getBooleanExtra("EXIT", false)) {
            finish()
            return
        }
        //endregion EXIT_APPLICATION
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
        val stb = AnimationUtils.loadAnimation(this, R.anim.stb)
        val ic_mw = findViewById<ImageView>(R.id.imgView_logo)
        ic_mw.startAnimation(stb)
    }

    companion object {
        private const val TAG = "EmailPassword"
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if(currentUser != null) {
            val intent = Intent(this, Menu::class.java)
            intent.putExtra("id", auth.currentUser!!.uid)
            startActivity(intent)
        }
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn:$email")
        if (!validateForm()) {
            return
        }
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)

                    val intent = Intent(this, Menu::class.java)
                    intent.putExtra("id", auth.currentUser!!.uid)
                    startActivity(intent)
                } else {
                    updateUI(null)
                    Toast.makeText(baseContext, "Login failed. Please try again.", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = etUsername.text.toString()
        if (TextUtils.isEmpty(email)) {
            etUsername.error = "Required."
            valid = false
        } else {
            etUsername.error = null
        }

        val password = etPassword.text.toString()
        if (TextUtils.isEmpty(password)) {
            etPassword.error = "Required."
            valid = false
        } else {
            etPassword.error = null
        }
        return valid
    }
}