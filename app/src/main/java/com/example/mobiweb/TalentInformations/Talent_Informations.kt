package com.example.mobiweb.TalentInformations

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.R
import com.example.mobiweb.Talent.Talent
import kotlinx.android.synthetic.main.talent_information.*
import java.text.SimpleDateFormat
import java.util.*

class Talent_Informations : BaseActivity() {
    @SuppressLint("SimpleDateFormat")

    private lateinit var viewModel: TalentViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.talent_information)

        viewModel = ViewModelProviders.of(this).get(TalentViewModel::class.java)
        val linkedInPackage: String = "com.linkedin.android"
        val intent = intent
        val valueId = intent.getStringExtra("id")
        val dateFormat = SimpleDateFormat("dd MM YYYY")

        viewModel.getDocumentRef(valueId)
        viewModel.document.observe(this, androidx.lifecycle.Observer {  it ->
            //txtTalentName.text = it.name

            it?.linkedIn?.let { value ->
                if (appInstalledOrNot(this, linkedInPackage)) {
                    btnLinkedIn.isEnabled = value.isNotEmpty()
                    btnLinkedIn.setOnClickListener {
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(value)
                        startActivity(i)
                    }
                } else {
                    btnLinkedIn.isEnabled = value.isNotEmpty()
                    btnLinkedIn.setOnClickListener {
                        val i = Intent(Intent.ACTION_VIEW)
                        i.data = Uri.parse(value)
                        startActivity(i)
                    }
                }

            }
            it.curriculumVitaePath.let { value ->
                btnCV.isEnabled = value.isNotEmpty()
                btnCV.setOnClickListener {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(value)
                    startActivity(i)
                }
            }

            it.coverLetter.let { value ->
                txtCoverLetter.text = value
                layoutCoverLetter.isVisible = value.isNotEmpty()
            }

            it.birthday.let { value ->
                txtTalentBirthday.text = value
                layoutTalentBirthday.isVisible = value.isNotEmpty()
            }

            it.cellPhoneNumber.let { value ->
                txtTalentPhoneNumber.text = value
                layoutTalentCellPhoneNumber.isVisible = value.isNotEmpty()
                layoutTalentCellPhoneNumber.setOnClickListener{
                    val callIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$value"))
                    startActivity(callIntent)
                }
            }

            it.email.let { value ->
                txtTalentEmail.text = value
                layoutTalentEmail.isVisible = value.isNotEmpty()
                layoutTalentEmail.setOnClickListener{
                    val callIntentEmail = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$value"))
                    callIntentEmail.putExtra(Intent.EXTRA_SUBJECT, "")
                    callIntentEmail.putExtra(Intent.EXTRA_TEXT, "")
                    startActivity(Intent.createChooser(callIntentEmail, "Chooser Title"))
                }
            }

            it.gender.let { value ->
                txtTalentGender.text = value
                layoutTalentGender.isVisible = value.isNotEmpty()
            }

            it.createdAt.let { value ->
                val milliseconds = value.seconds * 1000 + value.nanoseconds / 1000000
                val netDate = dateFormat.format(Date(milliseconds))

                txtTalentCreatedAt.text = netDate
                layoutTalentCreatedAt.isVisible = true
            }

            it.prefLocation.let { value ->
                txtTalentPrefLocation.text = value
                layoutTalentPrefLocation.isVisible = value.isNotEmpty()
            }

            it.source.let { value ->
                txtTalentSource.text = value
                layoutTalentSource.isVisible = value.isNotEmpty()
            }

            it.state.let { value ->
                txtTalentState.text = value
                layoutTalentState.isVisible = value.isNotEmpty()
            }

            it.title.let { value ->
                txtTalentPosition.text = value
                }
            it.name.let { value ->
                txtTalentName.text = value
            }
            isLoading(false, progressBar4)
        })
                }

    override fun onBackPressed() {
        val i = Intent(this, Talent::class.java)
        startActivity(i)
    }
            }

    fun appInstalledOrNot(context: Context, paramString: String): Boolean {
        val localPackageManager = context.packageManager
        try {
            localPackageManager.getPackageInfo(paramString, 0)
            return true
        } catch (ignored: PackageManager.NameNotFoundException) {
        }
        return false

    }

