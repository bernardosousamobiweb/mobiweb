package com.example.mobiweb.TalentInformations

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.mobiweb.Talent.Talent_Details
import com.google.firebase.firestore.FirebaseFirestore

class TalentViewModel (application: Application) : AndroidViewModel(application) {

    private val db = FirebaseFirestore.getInstance()
    val document : MutableLiveData<Talent_Details> = MutableLiveData()

    fun getDocumentRef(valueId: String?) {
        val docRef = db.collection("candidates").document(valueId.toString()).get()
            .addOnSuccessListener { snapshot ->
               val doc = snapshot.toObject(Talent_Details::class.java)

                        db.collection("jobs").document(snapshot.getString("jobUID").toString()).get()
                            .addOnSuccessListener { job  ->
                                if(doc?.jobUID == "") {
                                    doc.title = "Candidatura Espontânea"
                                    document.value = doc
                                } else {
                                    doc?.title = job.getString("title").toString()
                                    document.value = doc
                                }
                            }
                    }

            .addOnFailureListener { exception ->
                Log.d("Error db", "get failed with ", exception)
            }
    }
}