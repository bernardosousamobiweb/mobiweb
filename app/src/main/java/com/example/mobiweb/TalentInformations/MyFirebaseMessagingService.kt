package com.example.mobiweb.TalentInformations
import android.util.Log
import java.util.*
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import androidx.core.app.NotificationCompat
import com.example.mobiweb.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = "MyFirebaseToken"

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("TOKEN_UPDATE ", token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

         if(remoteMessage!!.notification != null)
        {
            val title = remoteMessage.notification!!.title
            val body = remoteMessage.notification!!.body
            var url : String? = ""
            if(!TextUtils.isEmpty(url))
            {
                val finalUrl = url
                var intent = Intent(this, Talent_Informations::class.java)

                Handler(Looper.getMainLooper())
                    .post {
                        showNotification(this@MyFirebaseMessagingService,
                            title,
                            body,
                            null)
                    }
            }
            else
            {
                var intent = Intent(this, Talent_Informations::class.java)
                showNotification(this@MyFirebaseMessagingService, title, body,null)
            }
        }
        else
        {
            val title = remoteMessage.data["title"]
            val body = remoteMessage.data["body"]
            val id = remoteMessage.data["candidateId"]
            val jobUID = remoteMessage.data["jobUID"]
            val identifier = remoteMessage.data["identifier"]
            val url : String? = ""
            if(TextUtils.isEmpty(url))
            {
                if(identifier == "harvest"){
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse("https://mobiweb.harvestapp.com/time")
                    Handler(Looper.getMainLooper())
                        .post {
                            showNotification(this@MyFirebaseMessagingService,
                                title,
                                body,
                                intent)
                        }
                } else {
                    var intent = Intent(this, Talent_Informations::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("jobUID", jobUID)
                    intent.putExtra("push", true)
                    Handler(Looper.getMainLooper())
                        .post {
                            showNotification(this@MyFirebaseMessagingService,
                                title,
                                body,
                                intent)
                        }
                }
            }
            else
            {
                var intent = Intent(this, Talent_Informations::class.java)
                showNotification(this@MyFirebaseMessagingService, title, body, null)
            }
        }

    }

    private fun showNotification(context: Context, title: String?, body: String?, intent: Intent?) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = Random().nextInt()
        val channelId = "mobiweb-intra"
        val channelName = "Mobiweb"

                val pendingIntent = PendingIntent.getActivity(
                    this,
                    100,
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT)

                if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
                {
                    val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
                    notificationManager.createNotificationChannel(channel)
                }

                val builder : NotificationCompat.Builder
                builder = NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.drawable.crop)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)

                if(intent != null)
                {
                    val stackBuilder = TaskStackBuilder.create(context)
                    stackBuilder.addNextIntent(intent)
                    val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                    builder.setContentIntent(resultPendingIntent)
                }

                notificationManager.notify(notificationId, builder.build())

            }


    }

