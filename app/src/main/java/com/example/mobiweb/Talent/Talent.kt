package com.example.mobiweb.Talent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.google.firebase.firestore.FirebaseFirestore
import android.util.Log
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.Menu.Menu
import com.example.mobiweb.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.content_main.*

class Talent : BaseActivity() {

    private lateinit var auth: FirebaseAuth
    private val users = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_main)
        auth = FirebaseAuth.getInstance()

        val thisActivity: Activity = this
        recyclerView.layoutManager = LinearLayoutManager(this@Talent)

        val db = FirebaseFirestore.getInstance()
        db.collection("candidates").orderBy("createdAt", Query.Direction.DESCENDING).get()
            .addOnCompleteListener { task ->
                 if(task.isSuccessful) { //           para todos os documents da coleccão candidatos
                     for (document in task.result!!) {
                         val talentName = document.getString("name")
                         val talentLocation = document.getString("prefLocation")
                         val talentState = document.getString("state")
                         var talentJob = document.getString("jobUID")

                         if (!talentJob.isNullOrEmpty()) {
                             db.collection("jobs").document(talentJob).get()
                                 .addOnSuccessListener { job ->
                                     users.add(
                                         User(
                                             talentName.toString(),
                                             job.getString("title").toString(),
                                             talentLocation.toString(),
                                             talentState.toString(),
                                             document.id
                                         )
                                     )
                                     recyclerView.adapter = UsersAdapter(
                                         users,
                                         applicationContext,
                                         thisActivity
                                     )
                                 }
                         } else {
                             users.add(
                                 User(
                                     talentName.toString(),
                                     "Candidatura Espontânea",
                                     talentLocation.toString(),
                                     talentState.toString(),
                                     document.id
                                 )
                             )
                         }
                     }
                 }
                top.visibility = VISIBLE
                layoutRecyclerView.visibility = VISIBLE
                isLoading(false, progressBar6)
            }
            .addOnFailureListener { exception ->
            Log.d("Error", "Error getting documents: ", exception)
        }
    }


    override fun onBackPressed() {
        val i = Intent(this, Menu::class.java)
        i.putExtra("id", auth.currentUser!!.uid)
        startActivity(i)
    }


    }