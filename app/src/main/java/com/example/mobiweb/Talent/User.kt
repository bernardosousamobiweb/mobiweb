package com.example.mobiweb.Talent

data class User(
    var name: String="",
    var job: String="",
    var prefLocation: String="",
    var state: String="",
    var id: String=""
)