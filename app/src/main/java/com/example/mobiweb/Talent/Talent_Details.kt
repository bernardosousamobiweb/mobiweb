package com.example.mobiweb.Talent

import com.google.firebase.Timestamp
import java.io.Serializable


data class Talent_Details (var name: String = "", var jobUID: String = "", var linkedIn: String = "", var curriculumVitaePath: String = "", var coverLetter: String = "", var birthday: String ="", var cellPhoneNumber: String="", var email: String ="", var gender: String="",
                           var createdAt : Timestamp = Timestamp.now(), var prefLocation: String="", var source: String="", var state: String="", var title: String="") : Serializable {

    constructor() : this("","","","","","","","","",Timestamp.now(),"","","", "")

}