package com.example.mobiweb.Talent

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.mobiweb.R
import com.example.mobiweb.TalentInformations.Talent_Informations
import kotlinx.android.synthetic.main.list_item.view.*

class UsersAdapter (private val usersList: MutableList<User>, private val mContext: Context, private val mActivity: Activity) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = usersList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        YoYo.with(Techniques.FadeIn).playOn(holder.cardView)
        YoYo.with(Techniques.FadeIn).playOn(holder.img)

        val user = usersList[position]
                holder.name.text = user.name
                holder.job.text = user.job
                holder.prefLocation.text = user.prefLocation
                holder.state.text = user.state
                holder.img.setImageResource(R.drawable.talentspic)

        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, Talent_Informations::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("id", user.id)
            intent.putExtra("jobUID", user.job)
            mContext.startActivity(intent)
        }

    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        val name: TextView = itemView.name_text
        val job: TextView = itemView.jobUID_text
        val prefLocation: TextView = itemView.pref_text
        val state: TextView = itemView.state_text
        val cardView: CardView = itemView.cardView
        var img: ImageView = itemView.img

    }

}