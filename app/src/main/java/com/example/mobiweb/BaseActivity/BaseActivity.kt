package com.example.mobiweb.BaseActivity

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

open class BaseActivity : AppCompatActivity() {

    private lateinit var viewModel : BaseViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)
        viewModel.loading.observe(this, Observer { isLoading(it.first, it.second) })
    }

    fun isLoading(i : Boolean, pg : RelativeLayout) {
        if(i)
            pg.visibility = View.VISIBLE
        else
            pg.visibility = View.GONE
    }

    fun copyText(text: String) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("copiedText", text)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Copied!", Toast.LENGTH_SHORT).show()
    }
}

