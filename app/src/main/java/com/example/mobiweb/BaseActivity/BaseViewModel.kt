package com.example.mobiweb.BaseActivity

import android.app.Application
import android.widget.RelativeLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class BaseViewModel (application: Application) : AndroidViewModel(application){
    val loading : MutableLiveData<Pair<Boolean,RelativeLayout>> = MutableLiveData()

}