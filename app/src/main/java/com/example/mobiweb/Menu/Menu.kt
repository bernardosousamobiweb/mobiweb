package com.example.mobiweb.Menu

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.mobiweb.Account.Account
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.Contract.Contract
import com.example.mobiweb.MainActivity
import com.example.mobiweb.Professional.ProfessionalData
import com.example.mobiweb.R
import com.example.mobiweb.Talent.Talent
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.menu.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NAME_SHADOWING")
class Menu : BaseActivity() {

    private lateinit var mTextView: TextView
    private lateinit var auth: FirebaseAuth
    private lateinit var strToken : String
    private lateinit var viewModel: MenuViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        val attrib = window.attributes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }

        val intent = intent
        val valueId = intent.getStringExtra("id")
        viewModel = ViewModelProviders.of(this).get(MenuViewModel::class.java)

        viewModel.getToken(valueId)
        viewModel.strToken.observe(this, Observer { it ->
            strToken = it
        })

        viewModel.subscribeToTopic()
        viewModel.isSubscribed.observe(this, Observer {
            Log.d("TAG-SUBSCRIBED:", it.toString())
        })

        mTextView = findViewById(R.id.dashQuote)
        showRandomFact()

        auth = FirebaseAuth.getInstance()

        val id = auth.currentUser!!.uid

        viewModel.defineMenu(id)
        viewModel.objectMenu.observe(this, Observer {
            val getTheName: String? = it.fullName
            val names = getTheName?.split(' ')
            val firstName = names?.get(0)
            txtDashWelcome.text = "Olá ${firstName}!"

            it?.avatarPath?.let { value ->
                Glide.with(applicationContext)
                    .load(value)
                    .apply(RequestOptions.circleCropTransform())
                    .error(R.drawable.dashaccount2)
                    .listener(object : RequestListener<Drawable>{
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            isLoading(false, progressBar1)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            isLoading(false, progressBar1)
                            return false
                        }

                    })
                    .into(dashavatar)
            }

            it?.role?.let { value ->
                if(value == "admin"){
                    btn_Projects.visibility = View.GONE
                    btn_Talent.visibility = View.VISIBLE
                    isLoading(false, progressBar1)


                }
            }

            //region BUTTONS
            btn_Account.setOnClickListener { value ->
                val intent = Intent(this, Account::class.java)
                intent.putExtra(Account.ACCOUNT_JSON, it)
                startActivity(intent)
            }

            btn_Professional.setOnClickListener {value ->
                val intent = Intent(this, ProfessionalData::class.java)
                intent.putExtra(ProfessionalData.PROFESSIONAL_JSON, it)
                startActivity(intent)
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }

            btn_Contract.setOnClickListener {value ->
                val intent = Intent(this, Contract::class.java)
                intent.putExtra(Contract.CONTRACT_JSON, it)
                startActivity(intent)
            }

            btn_Talent.setOnClickListener{value ->
                val intent = Intent(this, Talent::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }

            btn_Logout.setOnClickListener {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                FirebaseAuth.getInstance().signOut()
                finish()
            }
            //endregion
        })

        //region EXIT
        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish()
            return
        }
        //endregion
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.putExtra("EXIT", true)
            startActivity(intent)
        }
        return super.onKeyDown(keyCode, event)
    }

    private var factArray = arrayListOf(
        R.string.quote1,
        R.string.quote2,
        R.string.quote3,
        R.string.quote4,
        R.string.quote5,
        R.string.quote6,
        R.string.quote7,
        R.string.quote8
    )

    private fun shuffleFacts() {
        factArray.shuffle()
    }

    private fun showRandomFact() {
        shuffleFacts()
        mTextView.setText(factArray[0])
    }

    companion object {
        private const val TAG = "TAG"
    }

}
