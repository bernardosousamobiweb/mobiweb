package com.example.mobiweb.Menu

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.mobiweb.Account.Account_Details
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging

class MenuViewModel (application: Application) : AndroidViewModel(application){
    val strToken : MutableLiveData<String> = MutableLiveData()
    val objectMenu : MutableLiveData<Account_Details> = MutableLiveData()
    var isSubscribed : MutableLiveData<Boolean> = MutableLiveData()


    fun getToken(valueId: String){
        FirebaseInstanceId.getInstance()
            .instanceId
            .addOnCompleteListener { task ->
                if (task.isSuccessful)
                    Log.d("TOKEN", task.result!!.token)
                strToken.value = task.result!!.token

                val dbToken = FirebaseFirestore.getInstance()
                val ref = dbToken.collection("employees").document(valueId)
                ref.update("token", strToken.value)
                    .addOnSuccessListener {
                        Log.d("TAG", "Token: " + strToken + "added to firebase")
                    }
                    .addOnFailureListener {
                            e -> Log.w("TAG2", "error: ", e)
                    }
            }
    }

    fun subscribeToTopic(){
        FirebaseMessaging.getInstance().subscribeToTopic("harvest")
            .addOnCompleteListener { task ->
                var msg = "Subscribed"
                isSubscribed.value = true
                if (!task.isSuccessful) {
                    msg = "Not subscribed"
                    isSubscribed.value = false
                }
                Log.d("TAG", msg)
            }
    }


    fun defineMenu(id: String) {
        val db = FirebaseFirestore.getInstance()
        val docRef = db.collection("employees").document(id)

        docRef.get()
            .addOnSuccessListener { document ->
                val r = document.toObject(Account_Details::class.java)
                objectMenu.value = r
            }
    }

}