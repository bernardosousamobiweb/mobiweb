package com.example.mobiweb.Account

import java.io.Serializable

data class Account_Details(val civilState: String? = "", val gender: String? = "",
                           val employeeCode: String?="", val fullName: String?="",
                           val country: String?="", val address: String?="",
                           val birthday: String?="", val identificationCard : String?,
                           val numberOfDependents: String?="", val cellPhoneNumber: String?="",
                           val phoneNumber: String?="", val category: String?="", val role: String?="",
                           val socialSecurity: String?="", val taxNumber: String?="",
                           val zipCode: String?="", val email: String?="", val avatarPath: String?="",
                           val contract: String?="", val salary: String?="", val startDate: String?="",
                           val endDate: String?="", val renovationDate: String?="",
                           val insurancePolicy: String="", val ensuranceCompany: String="",
                           val ensuranceExpirationDate: String="", val workOffice: String="",
                           val department: String="", val schoolHabilitations: String="",
                           var jobUID: String="", var linkedIn: String="", var curriculumVitaePath: String="",
                           var coverLetter: String="",
                           var prefLocation: String="", var source: String="", var state: String="") : Serializable {

    constructor() : this("", "", "", "", "", "", "",
        null, "", "", "", "", "",
        "", "", "", "", "","","","",
        "","", "", "", "",
        "", "", "", "", "", "", "", "", "", "")
}
