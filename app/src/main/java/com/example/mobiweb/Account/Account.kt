package com.example.mobiweb.Account

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import kotlinx.android.synthetic.main.account.*
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.mobiweb.BaseActivity.BaseActivity
import com.example.mobiweb.R
import java.io.Serializable

class Account : BaseActivity(), Serializable {

    private var accountDetails: Account_Details = Account_Details()

    companion object {
        const val ACCOUNT_JSON = "account"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.account)

        val intent = intent
        intent.getSerializableExtra(ACCOUNT_JSON).let {
            accountDetails = it as Account_Details
        }

        //region CLIPBOARD
        btnCopyEmail.setOnClickListener { copyText(txtEmail.text as String) }
        btnCopyBirthday.setOnClickListener { copyText(txtBirthday.text as String) }
        btnCopyAddress.setOnClickListener { copyText(txtAddress.text as String) }
        btnCopyCellPhone.setOnClickListener { copyText(txtCellPhoneNumber.text as String) }
        btnCopyPhoneNumber.setOnClickListener { copyText(txtPhoneNumber.text as String) }
        btnCopyIdentificationCard.setOnClickListener { copyText(txtIdentification.text as String) }
        btnCopySocialSecurity.setOnClickListener { copyText(txtSocialSecurity.text as String) }
        btnCopyTaxNumber.setOnClickListener { copyText(txtTaxNumber.text as String) }
        btnCopyZip.setOnClickListener { copyText(txtZip.text as String) }
        btnCopyEmployeeCode.setOnClickListener { copyText(txtEmployeeCode.text as String) }
        //endregion
        val radius = this.resources.getDimensionPixelSize(R.dimen.corner_radius)
        accountDetails.avatarPath.let { value ->
            Glide.with(this)
                .load(value)
                .transform(RoundedCorners(radius))
                .error(R.drawable.dashaccount2)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        isLoading(false, progressBar2)
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        isLoading(false, progressBar2)
                        return false
                    }

                })
                .into(avatar)

        }

        accountDetails.identificationCard?.let { value ->
            txtIdentification.text = value
            layoutIdentificationCard.isVisible = value.isNotEmpty()
        }

        accountDetails.address?.let { value ->
            txtAddress.text = value
            layoutAddress.isVisible = value.isNotEmpty()
        }

        accountDetails.email?.let { value ->
            txtEmail.text = value
            layoutEmail.isVisible = value.isNotEmpty()
        }

        accountDetails.birthday?.let { value ->
            txtBirthday.text = value
            layoutBirthday.isVisible = value.isNotEmpty()
        }

        accountDetails.civilState?.let { value ->
            txtCivilState.text = value
            layoutCivilState.isVisible = value.isNotEmpty()
        }

        accountDetails.country?.let { value ->
            txtCountry.text = value
            layoutCountry.isVisible = value.isNotEmpty()
        }

        accountDetails.employeeCode?.let { value ->
            txtEmployeeCode.text = value
            layoutEmployeeCode.isVisible = value.isNotEmpty()
        }

        accountDetails.gender?.let { value ->
            txtGender.text = value
            layoutGender.isVisible = value.isNotEmpty()
        }

        accountDetails.numberOfDependents?.let { value ->
            txtNumberOfDependents.text = value
            layoutNumberOfDependents.isVisible = value.isNotEmpty()
        }

        accountDetails.cellPhoneNumber?.let { value ->
            txtCellPhoneNumber.text = value
            layoutCellPhoneNumber.isVisible = value.isNotEmpty()
        }

        accountDetails.phoneNumber?.let { value ->
            txtPhoneNumber.text = value
            layoutPhoneNumber.isVisible = value.isNotEmpty()
        }

        accountDetails.role?.let { value ->
            txtRole.text = value
            layoutRole.isVisible = value.isNotEmpty()
        }

        accountDetails.socialSecurity?.let { value ->
            txtSocialSecurity.text = value
            layoutSocialSecurity.isVisible = value.isNotEmpty()
        }

        accountDetails.taxNumber?.let { value ->
            txtTaxNumber.text = value
            layoutTaxNumber.isVisible = value.isNotEmpty()
        }

        accountDetails.zipCode?.let { value ->
            txtZip.text = value
            layoutZipCode.isVisible = value.isNotEmpty()
        }

        //Mandatory, that's why we don't need to check the data
        txtProfile.text = accountDetails.fullName
        txtProfilePosition.text = accountDetails.category

    }
}


